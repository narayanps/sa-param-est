function run_jr_sim(j)
addpath(strcat(pwd,'/func'))
SNR= [1 3 5 10];
T=100000;
T_ignore=10000;
n_s=6;
n_p=2;
p = 90 + 30.*randn(1,T+T_ignore);
H=zeros(1,n_s+n_p);
H(2)=1; H(3)=-1; 
state_0=zeros(n_s,1);
state=zeros(T,n_s);
y=zeros(1,T);
f=@nmm_jr_pest;
f_nmm=@nmm_jr_ukf;
h=@h_meas;
dt=0.001;
B(1:30000)=70;
B(30001:T+T_ignore)=50;
b(1:70000)=50;
b(70001:T+T_ignore)=100;

state(1,:) = f(state_0, B(1),b(1), p(1), dt);
y(1) = h(H(1:n_s), state(1,:)');
for i=2:T+T_ignore
    state(i,:) = f(state(i-1,:)',B(i), b(i), p(i), dt);
    y(i) = h(H(1:n_s), state(i,:)');
end
y=y-mean(y);
sig_var = var(y);
R = sig_var./SNR(j);
noise = chol(R)*randn(1,T+T_ignore);
y=y+noise;
y=y(T_ignore+1:end);
plot(y);
Q0=1e-10.*eye(n_s+n_p);
Q0(7,7)=1e-2;
Q0(8,8)=1e-2;
R0=1;
x_hat0=zeros(n_s+n_p,1);
x_hat0(7,1) = 70;
x_hat0(8,1) = 50;
P_xx0=100*eye(n_s+n_p);
ukf_params.alpha=1;
ukf_params.beta=0;
ukf_params.kappa=0;%3-(n_s+n_p);
[~,~,x_aug_hat_s,P_xx_s,~, ~]=uks_em_nmm(ukf_params,f_nmm,h,x_hat0,P_xx0, y, H, Q0, R0, p);

%plot results
lw=2;
dt=0.001;
fs=1/dt;
t=1/fs:1/fs:T/fs;
col_true='#0D4D79';
col_est='#842916'; 
col_CI=[177 26 22]./255;
set(0,'defaultTextInterpreter','latex'); 
figure('units','normalized','outerposition',[0 0 1 1]);
plot(t,B(T_ignore+1:end), 'LineWidth', lw, 'color', col_true)
hold on
plot(t,x_aug_hat_s(7,:), 'LineWidth', lw, 'color', col_est)
var_ = squeeze(P_xx_s(7,7,:));
CI = 2*sqrt(var_);
patch([t fliplr(t)], [x_aug_hat_s(7,:)-CI' fliplr(x_aug_hat_s(7,:)+CI')], col_CI, 'FaceAlpha',0.3, 'EdgeColor','none')
ylabel('$B [mV]$')
xlabel('\textbf{Time} [s]')


figure('units','normalized','outerposition',[0 0 1 1]);
plot(t,b(T_ignore+1:end), 'LineWidth', lw, 'color', col_true)
hold on
plot(t,x_aug_hat_s(8,:), 'LineWidth', lw, 'color', col_est)
var_ = squeeze(P_xx_s(8,8,:));
CI = 2*sqrt(var_);
patch([t fliplr(t)], [x_aug_hat_s(8,:)-CI' fliplr(x_aug_hat_s(8,:)+CI')], col_CI, 'FaceAlpha',0.3, 'EdgeColor','none')
ylabel('$b [s^{-1}]$')
xlabel('\textbf{Time} [s]')
