function [lfp] = h_meas(H,y)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%AUTHOR : NARAYAN P SUBRAMANIYAM
%TAMPERE UNIVERSITY
%2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

addpath(genpath(strcat(pwd,'safe_R1.1')))
lfp = H*y; %y(1) - y(2);
end

