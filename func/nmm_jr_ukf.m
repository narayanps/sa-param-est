function [y_new] = nmm_jr_ukf(y, p)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%AUTHOR : NARAYAN P SUBRAMANIYAM
%TAMPERE UNIVERSITY
%2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

a=100;
A=6;
dt=0.001;
C(1) = 135;
B=y(7);
b=y(8);
C(2)=108;
C(3) = 33.75;
C(4)=33.5;

v_0=6;
v_P=v_0;
v_I=v_0;
G=0;
%pyramidal
y_new(1) = euler_f1(y(1), y(4), dt);
D = A*a*S(y(2)-y(3),v_P) ;
y_new(4) = euler_f2(D, a, y(4), y(1), dt );



%secondary pyramidal
y_new(2) = euler_f1(y(2), y(5), dt);
v_P_prime=v_0;
D = A*a*C(2)*S(C(1)*y(1),v_P_prime) + A*a*G*S(y(2)-y(3), v_P) + A*a*p;
y_new(5) = euler_f2(D, a, y(5), y(2), dt );

%inhibitory
y_new(3) = euler_f1(y(3), y(6), dt);
D = B*b*C(4)*S(C(3)*y(1),v_I) ;
y_new(6) = euler_f2(D, b, y(6), y(3), dt );
y_new(7) = B;
y_new(8) = b;


y_new=y_new';
function [z] = S(x,v)
        e0=2.5;
        r=0.56;
        z=2*e0/(1+exp(r*(v-x)));
end


function [y] = euler_f1(y,x,dt)
        y = y + dt*x;
end


function [y] = euler_f2(K,c,y,z,dt)
        y = y + dt*(K - 2*c*y - c^2*z);
end



function [z] = H(x,k)
        z=x/(x+k);
end

end

