function [mi, sigma] = EE_analysis_JR()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%AUTHOR : NARAYAN P SUBRAMANIYAM
%TAMPERE UNIVERSITY
%2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

addpath(genpath(strcat(pwd,'safe_R1.1')))
M=13;
xmin = [6  30  0   1   10  10  50  50  30  30  5 2 0.5];
xmax = [10 70 200 1000 150 150 600 600 600 600 7 3 0.6];
DistrFun  = 'unif'  ;                                                          
DistrPar = cell(M,1);                                                          
for i=1:M 
    DistrPar{i} = [ xmin(i) xmax(i) ] ; 
end                             
%param_labels = {'A','B', 'mean', 'std', 'a', 'b', 'C1','C2','C3' ,'C4' } ;
r = 1000 ; % Number of Elementary Effects                                                                                                                                           
SampStrategy = 'lhs' ; 
design_type = 'radial';                                                        
X = OAT_sampling(r,M,DistrFun,DistrPar,SampStrategy,design_type);  
n_s=6; %number of states in NMM JR
T_ignore=10000;
T=100000;
T_tot=T+T_ignore;
state(:,1) = zeros(n_s, 1);
dt=0.001;
H=[0 1 -1 0 0 0 0 ];
h=@h_meas;
tic
parfor itr=1:length(X)
state = zeros(n_s, 1);
Y_tot=[];
for t=2:T_tot
    state= nmm_jr(X(itr,:)', state, dt);
    Y_tot = [Y_tot; h(H(1:n_s), state)];
end
Y(:,itr)=Y_tot(T_ignore+1:end);
end
toc
design_type='radial';
for t=1:size(Y,1)
[ mi(t,:), sigma(t,:) ] = EET_indices(r,xmin,xmax,X,Y(t,:)',design_type);
end
end