function [Si, STi] = sobol_analysis_JR_vbsa()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%AUTHOR : NARAYAN P SUBRAMANIYAM
%TAMPERE UNIVERSITY
%2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

addpath(genpath(strcat(pwd,'safe_R1.1')))
M=13;
xmin = [6  30 0  1   10 10  50 50  30  30  5 2 0.5];
xmax = [10 70 200 1000 150 150 600 600 600 600 7 3 0.6];
DistrFun  = 'unif'  ;                                                          
DistrPar = cell(M,1);                                                          
for i=1:M 
    DistrPar{i} = [ xmin(i) xmax(i) ] ; 
end                                                                                 
SampStrategy = 'lhs' ;
N = 15000 ; % Base sample size.
% Comment: the base sample size N is not the actual number of input 
% samples that will be evaluated. In fact, because of the resampling
% strategy, the total number of model evaluations to compute the two
% variance-based indices is equal to N*(M+2) 
X = AAT_sampling(SampStrategy,M,DistrFun,DistrPar,2*N);
[ XA, XB, XC ] = vbsa_resampling(X) ;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
n_s=6; %number of states in NMM JR
T_ignore=10000;
T=100000;
T_tot=T+T_ignore;

dt=0.001;
H=[0 1 -1 0 0 0 0 ];
h=@h_meas;
rng('shuffle')% for NMM stochastic noise

tic
parfor itr=1:length(XA)
state = zeros(n_s, 1);
Y_tot=[];
for t=2:T_tot
    state= nmm_jr(XA(itr,:)', state, dt);
    Y_tot = [Y_tot; h(H(1:n_s), state)];
end
YA(:,itr)=Y_tot(T_ignore+1:end);
end
toc

tic
parfor itr=1:length(XB)
state = zeros(n_s, 1);
Y_tot=[];
for t=2:T_tot
    state= nmm_jr(XB(itr,:)', state, dt);
    Y_tot = [Y_tot; h(H(1:n_s), state)];
end
YB(:,itr)=Y_tot(T_ignore+1:end);
end
toc

parfor itr=1:length(XC)
state = zeros(n_s, 1);
Y_tot=[];
for t=2:T_tot
    state= nmm_jr(XC(itr,:)', state, dt);
    Y_tot = [Y_tot; h(H(1:n_s), state)];
end
YC(:,itr)=Y_tot(T_ignore+1:end);
end
for j=1:size(YA,1)
[ Si(:,j), STi(:,j) ] = vbsa_indices(YA(j,:)',YB(j,:)',YC(j,:)');
end

% save('Si_100323.mat','Si');
% save('STi_100323.mat','STi')
end
