function test_jr_real_data_human()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%AUTHOR : NARAYAN P SUBRAMANIYAM
%TAMPERE UNIVERSITY
%2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

addpath(genpath(strcat(pwd,'safe_R1.1')))
close all
addpath(strcat(pwd,'/func'))
addpath(strcat(pwd,'/func'))
addpath(strcat(pwd,'/seizure_data'))

load seizure_data
y=seizure_data;
T=length(y);
n_s=6;
n_p=2;
p = 90 + 30.*randn(1,T);
H=zeros(1,n_s+n_p);
H(2)=1; H(3)=-1; 
f_nmm=@nmm_jr_ukf;
h=@h_meas;
Q0=1e-10*eye(n_s+n_p);
Q0(7,7)=1e-2;
Q0(8,8)=1e-2;
R0=1;
x_hat0=zeros(n_s+n_p,1);
x_hat0(7,1) = 70;
x_hat0(8,1) = 50;
P_xx0=100*eye(n_s+n_p);
ukf_params.alpha=1;
ukf_params.beta=0;
ukf_params.kappa=0;%3-(n_s+n_p);
[x_aug_hat_f,P_xx_f,x_aug_hat_s,P_xx_s,~, ~]=uks_em_nmm(ukf_params,f_nmm,h,x_hat0,P_xx0, y', H, Q0, R0, p);

%%plot results
set(0,'defaultTextInterpreter','latex'); %trying to set the default
lw=2;
fsize=32;

col_line='#842916';
col_data='#0D4D79';
col_est='#842916'; 
col_CI=[177 26 22]./255;


figure('units','normalized','outerposition',[0 0 1 1])
subplot(3,1,1)
plot(t,seizure_data/1000, 'LineWidth', 2, 'color', col_data)
xline(61.5, 'LineWidth', 3, 'color', col_line)
hold on
xlabel('Time [s]')
ylabel('Amplitude $[mV]$')
set(gca,'fontsize',fsize,'FontWeight','bold')

subplot(3,1,2)

B=x_aug_hat_f./30 + 70;
plot(t,B, 'LineWidth', 2, 'color', col_est)
var = squeeze(P_xx_f(7,7,:));
CI = 2*(sqrt(var)./30);
patch([t fliplr(t)], [B-CI' fliplr(B+CI')], col_CI, 'FaceAlpha',0.5, 'EdgeColor','none')
hold on
ylim([30 90])
ylabel('$B [mV]$')
xlabel('Time [s]')
set(gca,'fontsize',fsize,'FontWeight','bold')

subplot(3,1,3) 
b=x_aug_hat_f(8,:)./30 + 50;
plot(t,b, 'LineWidth', 2, 'color', col_est)
var = squeeze(P_xx_f(8,8,:));
CI = 2*(sqrt(var)./30);
patch([t fliplr(t)], [b-CI' fliplr(b+CI')], col_CI, 'FaceAlpha',0.5, 'EdgeColor','none')
ylim([30 80])
ylabel('$b [s^{-1}]$')
xlabel('Time [s]')
set(gcf, 'unit', 'inches');
h_legend = legend({ 'Estimated mean', '$2\sigma$ CI'}, 'Location', 'northeast');
set(h_legend, 'location', 'northeast','Box', 'off')
end
